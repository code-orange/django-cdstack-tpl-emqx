## NOTE:
## Configs in this file might be overridden by:
## 1. Environment variables which start with 'EMQX_' prefix
## 2. File $EMQX_NODE__DATA_DIR/configs/cluster-override.conf
## 3. File $EMQX_NODE__DATA_DIR/configs/local-override.conf
##
## The *-override.conf files are overwritten at runtime when changes
## are made from EMQX dashboard UI, management HTTP API, or CLI.
## All configuration details can be found in emqx.conf.example

node {
  name = "emqx@127.0.0.1"
  cookie = "emqxsecretcookie"
  data_dir = "/var/lib/emqx"
}

log {
  file_handlers.default {
    level = warning
    file = "/var/log/emqx/emqx.log"
  }
}

cluster {
  name = emqxcl
  discovery_strategy = manual
}


listeners.tcp.default {
  bind = "127.0.0.1:21883"
  max_connections = 1024000
  proxy_protocol = true
}

listeners.ws.default {
  bind = "127.0.0.1:28083"
  max_connections = 1024000
  websocket.mqtt_path = "/mqtt"
}

dashboard {
    listeners.http {
        bind = 18083
    }
    default_username = "admin"
    default_password = "public"
}

authorization {
  deny_action = ignore
  no_match = allow
  cache = { enable = true }
  sources =  [
    {
      type = file
      enable = true
      # This file is immutable to EMQX.
      # Once new rules are created from dashboard UI or HTTP API,
      # the file 'data/authz/acl.conf' is used instead of this one
      path = "/etc/emqx/acl.conf"
    }
  ]
}
